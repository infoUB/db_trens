/*----------------------------
			QUERY 1
------------------------------*/
SELECT p.id_persona, p.nom FROM persones AS p
JOIN viatges AS v ON v.id_persona = p.id_persona
JOIN desplacaments AS d ON v.id_viatge = d.id_viatge
JOIN trajectes AS t ON d.id_trajecte = t.id_trajecte
JOIN parades AS s ON t.id_trajecte = s.id_trajecte
JOIN estacions AS e ON e.id_estacio = s.id_estacio
WHERE e.id_estacio = 71801 AND t.data = '2021-05-19';

/*----------------------------
			QUERY 2
------------------------------*/
SELECT id_persona AS Usuari, GROUP_CONCAT(DISTINCT (estacions.nom) SEPARATOR ', ') AS Estacions FROM
(
    (
        SELECT id_persona, desplacaments.estacio_arribada, desplacaments.estacio_sortida
        FROM desplacaments
        INNER JOIN viatges ON viatges.id_viatge = desplacaments.id_viatge
    ) AS cc
    INNER JOIN estacions ON (estacio_arribada = estacions.id_estacio OR estacio_sortida = estacions.id_estacio)
)
GROUP BY id_persona;

/*----------------------------
			QUERY 3
------------------------------*/
SELECT * FROM en_servei WHERE id_unitat IN 
	(SELECT unitats.id_unitat  FROM unitats
	JOIN trens_unitats ON trens_unitats.id_unitat = unitats.id_unitat
	JOIN trens ON trens_unitats.id_tren = trens.id_tren
	JOIN trajectes on trajectes.id_tren = trens.id_tren
	WHERE trajectes.data >= '2016-1-1' AND trajectes.data <='2022-1-1'
	GROUP BY id_unitat HAVING COUNT(id_trajecte) < 2)
ORDER BY data_prevista_baixa;

/*----------------------------
			QUERY 4
------------------------------*/
SELECT l.id_linia,COUNT(id_persona) AS num_clients FROM linies AS l 
JOIN serveis AS s ON s.id_linia = l.id_linia
JOIN trajectes AS t ON t.id_servei = s.id_servei
JOIN desplacaments AS d ON t.id_trajecte = d.id_trajecte
JOIN viatges AS v ON v.id_viatge = d.id_viatge
GROUP BY id_linia
ORDER BY num_clients
LIMIT 3;

/*----------------------------
			QUERY 5
------------------------------*/
SELECT unitats.id_unitat, SUM(serveis.longitud) AS distancia_km FROM unitats 
JOIN trens_unitats ON unitats.id_unitat = trens_unitats.id_unitat
JOIN trens ON trens_unitats.id_tren = trens.id_tren
JOIN trajectes ON trens.id_tren = trajectes.id_tren AND trajectes.data < CURRENT_DATE()
JOIN serveis ON trajectes.id_servei = serveis.id_servei 
GROUP BY id_unitat ORDER BY distancia_km DESC;

/*----------------------------
			QUERY 6
------------------------------*/
SELECT id_serie, COUNT(f.id_unitat) AS unitats_fora_servei, COUNT(e.id_unitat) AS unitats_en_servei
FROM series AS s
JOIN unitats AS u ON s.id_series = u.id_serie
LEFT JOIN fora_servei AS f ON f.id_unitat = u.id_unitat
LEFT JOIN en_servei AS e ON e.id_unitat = u.id_unitat
GROUP BY id_serie;


/*----------------------------
			QUERY 7
------------------------------*/
SELECT hora_sortida FROM parades WHERE
(
    id_trajecte = '5617R1' AND
    id_estacio = 
    (
        SELECT serveis.estacio_inici FROM serveis WHERE 
        (
            id_servei = (SELECT id_servei FROM trajectes WHERE id_trajecte = '5617R1')
        )
    )
) into @sortida;
SELECT hora_arribada FROM parades WHERE 
(
    id_trajecte = '5617R1' AND
    id_estacio = 
    (
        SELECT serveis.estacio_final FROM serveis WHERE 
        (
            id_servei = (SELECT id_servei FROM trajectes WHERE id_trajecte = '5617R1')
        )
    )
) INTO @arribada;
SELECT TIMEDIFF(@arribada, @sortida) as durada;

/*----------------------------
			QUERY 8
------------------------------*/
SELECT id_servei, GROUP_CONCAT(estacions.nom) as estacions FROM
(
	(SELECT * FROM estacions WHERE id_estacio = '77303') AS estacio_inici
	JOIN estacions_linies E ON estacio_inici.id_estacio = E.id_estacio
	JOIN serveis S ON S.id_linia = E.id_linia
	JOIN estacions ON S.estacio_inici = estacions.id_estacio OR S.estacio_final = estacions.id_estacio)
	WHERE
	(
		E.ordre >= (select ordre from estacions_linies where estacions_linies.id_linia = E.id_linia AND estacions_linies.id_estacio = S.estacio_inici) 
		AND 
		E.ordre <= (select ordre from estacions_linies where estacions_linies.id_linia = E.id_linia AND estacions_linies.id_estacio = S.estacio_final)
	)
GROUP BY id_servei;

/*----------------------------
			QUERY 9,10,11
------------------------------*/
START TRANSACTION;
	DELETE FROM trens_unitats WHERE trens_unitats.id_tren IN 	
			(SELECT trens.id_tren 
			FROM trajectes RIGHT JOIN trens ON trens.id_tren = trajectes.id_tren
			GROUP BY trens.id_tren HAVING COUNT(id_trajecte) = 0);
	DELETE FROM trens WHERE id_tren = ANY(SELECT * FROM	
			(SELECT trens.id_tren 
			FROM trajectes RIGHT JOIN trens ON trens.id_tren = trajectes.id_tren
			GROUP BY trens.id_tren HAVING COUNT(id_trajecte) = 0)AS a);
	SELECT id_unitat FROM en_servei S WHERE S.id_unitat NOT IN (SELECT id_unitat FROM trens_unitats); 
	INSERT INTO trens (id_tren) VALUES('11X0A');
	INSERT INTO trens_unitats (id_tren, id_unitat) 
		SELECT '11X0A', id_unitat 
        FROM en_servei S 
        WHERE S.id_unitat NOT IN (SELECT id_unitat FROM trens_unitats) LIMIT 2;
        
ROLLBACK;

/*----------------------------
			QUERY 12
------------------------------*/
SELECT aux.nom, aux.id_persona FROM 
	(
	SELECT p.nom, p.id_persona, t.data as data FROM persones AS p
	JOIN viatges AS v ON p.id_persona = v.id_persona
	LEFT JOIN desplacaments AS d ON d.id_viatge = v.id_viatge
	JOIN trajectes AS t ON t.id_trajecte = d.id_trajecte
    ) AS aux
GROUP BY aux.id_persona
HAVING MAX(aux.data) < '2021-05-20';


/*----------------------------
			QUERY 13
------------------------------*/
SELECT p.nom, p.id_persona FROM persones AS p
JOIN viatges AS v ON p.id_persona = v.id_persona
JOIN desplacaments AS d ON v.id_viatge = d.id_viatge
JOIN trajectes AS t ON t.id_trajecte = d.id_trajecte
WHERE t.data >='2021-05-20' AND t.data <= '2021-06-20'
GROUP BY p.nom, p.id_persona
HAVING COUNT(v.id_viatge) > 1;