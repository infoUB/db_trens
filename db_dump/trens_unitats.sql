CREATE DATABASE  IF NOT EXISTS `trens` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `trens`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: trens
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `unitats`
--

DROP TABLE IF EXISTS `unitats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unitats` (
  `id_unitat` varchar(10) NOT NULL,
  `any_fabricacio` year NOT NULL,
  `fabricant` varchar(45) DEFAULT NULL,
  `id_serie` int NOT NULL,
  PRIMARY KEY (`id_unitat`),
  KEY `unitat_serie_idx` (`id_serie`),
  CONSTRAINT `unitat_serie` FOREIGN KEY (`id_serie`) REFERENCES `series` (`id_series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unitats`
--

LOCK TABLES `unitats` WRITE;
/*!40000 ALTER TABLE `unitats` DISABLE KEYS */;
INSERT INTO `unitats` VALUES ('002B',2004,'CAF',464),('008G',1992,'Alstom',450),('027G',1998,'CAF',447),('053G',2008,'CAF',464),('062G',1991,'CAF',450),('091K',2006,'Alstom',463),('109Q',1974,'CAF',470),('109T',1998,'Alstom',447),('119R',1987,'CAF',448),('162A',1998,'CAF',447),('186V',1974,'CAF',470),('232Q',1974,'Macosa',440),('234Z',2010,'Alstom',464),('237T',1974,'Macosa',470),('290J',1974,'CAF',440),('397I',2008,'CAF',449),('411P',1993,'ABB',447),('483G',1974,'Macosa',440),('484R',1987,'Ateinsa',448),('494E',1987,'CAF',448),('510D',1974,'Macosa',470),('515M',2008,'Bombardier',464),('526M',1987,'MTM',448),('526U',2008,'CAF',449),('537Z',2008,'CAF',449),('543K',2004,'Bombardier',464),('558Z',1974,'Macosa',470),('579U',2009,'Bombardier',465),('589S',1995,'Alstom',447),('597U',1974,'CAF',440),('667A',2005,'Alstom',465),('669O',1997,'Siemens',447),('686S',2000,'CAF',447),('701Q',2006,'Bombardier',464),('712Q',1989,'Alstom',451),('755T',1987,'Ateinsa',448),('756J',1992,'Alstom',450),('777X',1974,'Macosa',470),('796B',2005,'Siemens',464),('806Z',2008,'CAF',464),('808K',1974,'CAF',470),('818U',1974,'Macosa',470),('819D',1974,'Macosa',470),('830X',1994,'Alstom',450),('835Z',2010,'CAF',449),('848H',1987,'CAF',448),('853C',1988,'Alstom',450),('900L',1987,'Ateinsa',448),('907E',1974,'CAF',470),('916H',1974,'Macosa',470),('924Q',2010,'CAF',449),('935V',1996,'Alstom',447),('944Z',2008,'CAF',449),('950M',1987,'Macosa',448),('959E',1990,'Alstom',450),('960L',2010,'Bombardier',465),('984N',1987,'Macosa',448),('988N',2007,'CAF',464),('995F',1987,'Ateinsa',448);
/*!40000 ALTER TABLE `unitats` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-11 20:00:01
