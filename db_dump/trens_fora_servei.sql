CREATE DATABASE  IF NOT EXISTS `trens` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `trens`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: trens
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fora_servei`
--

DROP TABLE IF EXISTS `fora_servei`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fora_servei` (
  `id_unitat` varchar(10) NOT NULL,
  `motiu` text,
  `data_baixa` date DEFAULT NULL,
  PRIMARY KEY (`id_unitat`),
  KEY `foraservei_unitat_idx` (`id_unitat`),
  CONSTRAINT `foraservei_unitat` FOREIGN KEY (`id_unitat`) REFERENCES `unitats` (`id_unitat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fora_servei`
--

LOCK TABLES `fora_servei` WRITE;
/*!40000 ALTER TABLE `fora_servei` DISABLE KEYS */;
INSERT INTO `fora_servei` VALUES ('027G','Taller de pintura','2011-08-08'),('053G','Manteniment pantògrafs','2016-12-22'),('091K','Fallada de comunicació TIERRA-TREN','2018-10-17'),('597U','Fallada bojie de la unitat central','2016-11-23'),('669O','Fallada de comunicació TIERRA-TREN','2017-04-04'),('686S','Manteniment pantògrafs','2020-09-30'),('712Q','Fallada del sistema de climatització','2021-01-07'),('848H','Fallada de comunicació TIERRA-TREN','2018-11-04'),('916H','Fallada bojie de la unitat central','2009-01-03'),('944Z','Restauració','2017-04-07'),('959E','Fallada bojie de la unitat central','2020-10-12');
/*!40000 ALTER TABLE `fora_servei` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-11 20:00:00
