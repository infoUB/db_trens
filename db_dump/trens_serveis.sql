CREATE DATABASE  IF NOT EXISTS `trens` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `trens`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: trens
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `serveis`
--

DROP TABLE IF EXISTS `serveis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `serveis` (
  `id_servei` varchar(15) NOT NULL,
  `estacio_inici` int NOT NULL,
  `estacio_final` int NOT NULL,
  `longitud` int NOT NULL,
  `id_linia` varchar(15) NOT NULL,
  PRIMARY KEY (`id_servei`),
  KEY `servei_linia_idx` (`id_linia`),
  KEY `servei_estacio_idx` (`estacio_inici`),
  KEY `servei_estacio_final_idx` (`estacio_final`),
  CONSTRAINT `servei_estacio_final` FOREIGN KEY (`estacio_final`) REFERENCES `estacions` (`id_estacio`),
  CONSTRAINT `servei_estacio_inici` FOREIGN KEY (`estacio_inici`) REFERENCES `estacions` (`id_estacio`),
  CONSTRAINT `servei_linia` FOREIGN KEY (`id_linia`) REFERENCES `linies` (`id_linia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serveis`
--

LOCK TABLES `serveis` WRITE;
/*!40000 ALTER TABLE `serveis` DISABLE KEYS */;
INSERT INTO `serveis` VALUES ('R1A',72305,79200,83,'R1'),('R1B',72305,79606,72,'R1'),('R1C',72305,79603,60,'R1'),('R1D',72305,79600,51,'R1'),('R1E',72305,79500,42,'R1'),('R1F',72300,79600,63,'R1'),('R1G',72300,79500,52,'R1'),('R2A',71600,71802,64,'R2S'),('R2B',71700,71802,44,'R2S'),('R2C',71705,79100,52,'R2'),('R2D',72400,79100,41,'R2N'),('R2E',72400,79104,61,'R2N'),('R2F',72400,79200,77,'R2N'),('R3A',72305,77006,57,'R3'),('R3B',72305,77102,66,'R3'),('R3C',72305,77109,94,'R3'),('R3D',72305,77200,122,'R3'),('R3E',72305,77200,122,'R3'),('R3F',72305,77303,132,'R3'),('R3G',72305,77309,161,'R3'),('R3H',72305,77309,161,'R3'),('R3I',72305,77309,161,'R3'),('R4A',71600,78600,143,'R4'),('R4B',72204,78600,132,'R4'),('R4C',72204,78710,107,'R4'),('R4D',72209,78710,89,'R4'),('R4E',72305,78710,64,'R4'),('R4F',72305,78600,92,'R4'),('R7A',78802,72503,15,'R7'),('R8A',72209,79100,40,'R8');
/*!40000 ALTER TABLE `serveis` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-11 20:00:01
