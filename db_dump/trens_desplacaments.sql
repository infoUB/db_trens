CREATE DATABASE  IF NOT EXISTS `trens` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `trens`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: trens
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `desplacaments`
--

DROP TABLE IF EXISTS `desplacaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `desplacaments` (
  `id_desplacament` varchar(15) NOT NULL,
  `id_viatge` varchar(15) NOT NULL,
  `estacio_sortida` int NOT NULL,
  `estacio_arribada` int NOT NULL,
  `id_trajecte` varchar(45) NOT NULL,
  `ordre` int unsigned NOT NULL,
  PRIMARY KEY (`id_desplacament`),
  KEY `desplaçament_viatge_idx` (`id_viatge`),
  KEY `desplacaments_trajectes_idx` (`id_trajecte`),
  KEY `desplacaments_estacio_idx` (`estacio_sortida`),
  KEY `desplacaments_estacio_final_idx` (`estacio_arribada`),
  CONSTRAINT `desplacament_viatge` FOREIGN KEY (`id_viatge`) REFERENCES `viatges` (`id_viatge`),
  CONSTRAINT `desplacaments_estacio_final` FOREIGN KEY (`estacio_arribada`) REFERENCES `estacions` (`id_estacio`),
  CONSTRAINT `desplacaments_estacio_inici` FOREIGN KEY (`estacio_sortida`) REFERENCES `estacions` (`id_estacio`),
  CONSTRAINT `desplacaments_trajectes` FOREIGN KEY (`id_trajecte`) REFERENCES `trajectes` (`id_trajecte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desplacaments`
--

LOCK TABLES `desplacaments` WRITE;
/*!40000 ALTER TABLE `desplacaments` DISABLE KEYS */;
INSERT INTO `desplacaments` VALUES ('D00001','V77937Q',72305,79406,'5617R1',1),('D00002','V08726D',79410,72300,'8800R1',1),('D00003','V36247Z',77006,78804,'5306R3',1),('D00004','V70855P',79007,72503,'8656R8',1),('D00005','V43387N',78704,72211,'5016R4',1),('D00006','V97153O',79600,71801,'8800R1',1),('D00007','V97153O',71801,79100,'5402R2N',2),('D00008','V84138H',77109,78805,'5360R3  ',1),('D00009','V84138H',78805,71801,'5622R4  ',2),('D00010','V67657S',72305,71801,'5363R3',1),('D00011','V67657S',71801,78802,'8006R4',2),('D00012','V67657S',78802,78800,'5829R7',3);
/*!40000 ALTER TABLE `desplacaments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-11 20:00:01
