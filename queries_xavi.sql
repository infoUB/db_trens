/*
SELECT id_client AS Client, GROUP_CONCAT(DISTINCT (estacions.nom) SEPARATOR ', ') AS Estacions FROM
	(
		(
			SELECT id_client, desplacaments.estacio_arribada, desplacaments.estacio_sortida
			FROM desplacaments
			INNER JOIN viatges ON viatges.id_viatge = desplacaments.id_viatge
        ) AS cc
		INNER JOIN estacions ON (estacio_arribada = estacions.id_estacio OR estacio_sortida = estacions.id_estacio)
    )
GROUP BY id_client;

SELECT * from estacions;

SELECT z.id_unitat FROM
	(SELECT * from unitats f
	WHERE f.id_unitat not in (SELECT id_unitat FROM trens_unitats)) AS dd
	JOIN fora_servei z ON dd.id_unitat = z.id_unitat;
    
START TRANSACTION;
	INSERT INTO unitats (id_unitat, any_fabricacio) VALUES ('A4A','1919');
    SELECT * from unitats;
ROLLBACK;

SELECT id_tren, GROUP_CONCAT(id_unitat) from trens_unitats GROUP BY id_tren;
*/

SELECT trens.id_tren as tren, COUNT(id_trajecte) as nombre_trajectes
FROM trajectes RIGHT JOIN trens ON trens.id_tren = trajectes.id_tren
GROUP BY trens.id_tren HAVING nombre_trajectes = 0;
    
START TRANSACTION;
	DELETE FROM trens_unitats WHERE trens_unitats.id_tren IN 	
			(SELECT trens.id_tren 
			FROM trajectes RIGHT JOIN trens ON trens.id_tren = trajectes.id_tren
			GROUP BY trens.id_tren HAVING COUNT(id_trajecte) = 0);
	DELETE FROM trens WHERE id_tren = ANY(SELECT * FROM	
			(SELECT trens.id_tren 
			FROM trajectes RIGHT JOIN trens ON trens.id_tren = trajectes.id_tren
			GROUP BY trens.id_tren HAVING COUNT(id_trajecte) = 0)AS a);
	SELECT id_unitat FROM en_servei S WHERE S.id_unitat NOT IN (SELECT id_unitat FROM trens_unitats); 
	INSERT INTO trens (id_tren) VALUES('11X0A');
	INSERT INTO trens_unitats (id_tren, id_unitat) 
		SELECT '11X0A', id_unitat 
        FROM en_servei S 
        WHERE S.id_unitat NOT IN (SELECT id_unitat FROM trens_unitats) LIMIT 2;
        
    SELECT * FROM trens_unitats;
ROLLBACK;


