import random
import rstr


viatges = set()
clients = set()
sum = 0

with open('viatges.csv','w') as file:
    file.write('id_viatge, id_client\n')
    while sum < 8:
        client_id = rstr.xeger(r'[0-9]{8}[A-Z]')
        while client_id in clients:
            client_id = rstr.xeger(r'[0-9]{8}[A-Z]')
            
        for i in range(random.randint(1,3)):
            if (sum < 8):
                sum += 1
                viatge_id = rstr.xeger(r'V[0-9]{5}[A-Z]')
                while viatge_id in viatges:
                    viatge_id = rstr.xeger(r'V[0-9]{5}[A-Z]')
                file.write(viatge_id+', '+client_id+'\n')
                
                
                
            
