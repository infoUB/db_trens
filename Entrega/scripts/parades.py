import random
import csv
import rstr

# $ cut -c7- stops_minimal.csv > stops_minimal_trimmed.csv
with open('stops_minimal_trimmed.csv', 'r') as orig:
    with open('stops_minimal_trimmed_via.csv', 'w') as nou:
        for line in orig:
            if line != '\n':
                line = line.rstrip('\n') + ", " + str(random.randint(1,3))
                print(line, file = nou)


# $ awk -F ',' '{OFS=FS; print $1, $2, $3, $4, $6}' stops_minimal_trimmed_via.csv > parades.csv