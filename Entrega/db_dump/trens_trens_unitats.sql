CREATE DATABASE  IF NOT EXISTS `trens` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `trens`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: trens
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `trens_unitats`
--

DROP TABLE IF EXISTS `trens_unitats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trens_unitats` (
  `id_tren` varchar(10) NOT NULL,
  `id_unitat` varchar(10) NOT NULL,
  PRIMARY KEY (`id_tren`,`id_unitat`),
  KEY `trenunitat_unitat_idx` (`id_unitat`),
  CONSTRAINT `trenunitat_tren` FOREIGN KEY (`id_tren`) REFERENCES `trens` (`id_tren`),
  CONSTRAINT `trenunitat_unitat` FOREIGN KEY (`id_unitat`) REFERENCES `unitats` (`id_unitat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trens_unitats`
--

LOCK TABLES `trens_unitats` WRITE;
/*!40000 ALTER TABLE `trens_unitats` DISABLE KEYS */;
INSERT INTO `trens_unitats` VALUES ('91X0F','002B'),('46X0B','008G'),('56X0O','062G'),('93X0X','109Q'),('14X0C','109T'),('25X0U','119R'),('14X0C','162A'),('33X0Z','186V'),('05X0K','232Q'),('96X0V','234Z'),('13X0P','237T'),('05X0K','290J'),('12X0Y','397I'),('17X0Q','411P'),('71X0G','483G'),('92X0U','484R'),('59X0R','494E'),('93X0X','510D'),('96X0V','515M'),('78X0L','526M'),('12X0Y','526U'),('77X0J','537Z'),('38X0Q','543K'),('08X0U','558Z'),('45X0Z','579U'),('14X0C','589S'),('45X0Z','667A'),('29X0T','701Q'),('92X0U','755T'),('56X0O','756J'),('24X0A','777X'),('91X0F','796B'),('96X0V','806Z'),('33X0Z','808K'),('36X0Z','818U'),('93X0X','819D'),('56X0O','830X'),('96X0R','835Z'),('78X0I','853C'),('59X0R','900L'),('33X0Z','907E'),('96X0R','924Q'),('17X0Q','935V'),('59X0R','950M'),('76X0M','960L'),('92X0U','984N'),('91X0F','988N'),('25X0U','995F');
/*!40000 ALTER TABLE `trens_unitats` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-11 20:00:01
