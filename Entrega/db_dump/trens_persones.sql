CREATE DATABASE  IF NOT EXISTS `trens` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `trens`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: trens
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `persones`
--

DROP TABLE IF EXISTS `persones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persones` (
  `id_persona` varchar(15) NOT NULL,
  `nom` varchar(150) NOT NULL,
  `dob` date NOT NULL,
  PRIMARY KEY (`id_persona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persones`
--

LOCK TABLES `persones` WRITE;
/*!40000 ALTER TABLE `persones` DISABLE KEYS */;
INSERT INTO `persones` VALUES ('13123768F','Francesc MartÃ­nez VilÃ ','2009-12-25'),('13147923U','Antonio LÃ³pez Serra','1993-05-10'),('29070702Z','Maria MartÃ­nez Serra','1969-07-11'),('39031765T','Anna LÃ³pez VilÃ ','1970-01-05'),('39315865E','David MartÃ­nez Ferrer','1989-11-22'),('39402116T','Maria MartÃ­nez Fuster','1996-10-14'),('39775852F','Anna GarcÃ­a Serra','2005-04-28'),('39984051Z','Maria GarcÃ­a Fuster','1970-03-19'),('39986214F','Sara LÃ³pez VilÃ ','1951-03-01'),('39994162T','Sara GarcÃ­a Fuster','1972-10-16'),('40155079E','Antonio LÃ³pez Fuster','2002-09-04'),('40218135H','David MartÃ­nez Fuster','1984-05-25'),('40270034H','Eric LÃ³pez Serra','2009-11-19'),('40331399E','Sara LÃ³pez VilÃ ','2001-05-05'),('40340223B','Antonio LÃ³pez Serra','1993-04-01'),('40629979H','Antonio LÃ³pez VilÃ ','1988-04-24'),('40754186F','David GarcÃ­a Serra','1947-12-10'),('40820422F','Eric MartÃ­nez VilÃ ','1956-08-25'),('84830205Q','David MartÃ­nez Ferrer','1973-09-15'),('99847657Y','Carlos GarcÃ­a Serra','1991-07-01');
/*!40000 ALTER TABLE `persones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-11 20:00:00
