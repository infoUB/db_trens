import random
import csv
import rstr
from datetime import date

min_unit_serie = 1 #Nombre d'unitats minim per serie
max_unit_serie = 10
defect_min_units_serie = 1
min_tren_length = 1


series = ["440", "447", "448", "449", "450", "451", "463", "464", "465", "470"]

# Generem unitats.csv, que conté les unitats de la línia

fabricants = {  "440":["CAF", "Macosa"], 
                "447":["Alstom", "Siemens", "CAF", "ABB", "Adtranz"],
                "448":["CAF", "Macosa", "Ateinsa", "MTM"],
                "449":["CAF"],
                "450":["CAF", "Alstom"],
                "451":["CAF", "Alstom"],
                "463":["CAF", "Alstom", "Siemens", "Bombardier"],
                "464":["CAF", "Alstom", "Siemens", "Bombardier"],
                "465":["CAF", "Alstom", "Siemens", "Bombardier"],
                "470":["CAF", "Macosa"]
            }

anys = {"440":[1974],
        "447":[x for x in range(1992, 2002)],
        "448":[1987],
        "449":[x for x in range(2008,2012)],
        "450":[x for x in range(1988, 1995)],
        "451":[x for x in range(1988, 1995)],
        "463":[x for x in range(2004,2011)],
        "464":[x for x in range(2004,2011)],
        "465":[x for x in range(2004,2011)],
        "470":[1974]
        }


used_id = set()
unit_series = { serie:[] for serie in series}
unit_id = ''

random.seed(3)


with open('unitats.csv', 'w') as file:
    file.write('id_unitat, id_serie, fabricant, any_fabricacio\n')
    for serie in series:
        l = random.randint(min_unit_serie,max_unit_serie)
        for i in range(l):
    
            unit_id = rstr.xeger(r'[0-9]{3}[A-Z]') #Que bonic quedaria això amb un do while...
            
            while (unit_id in used_id):
                unit_id = rstr.xeger(r'[0-9]{3}[A-Z]')
    
            used_id.add(unit_id)
            unit_series[serie].append(unit_id) #Guardem quins són de cada sèrie per després
    
            line = unit_id + ", " + serie + ", " + random.choice(fabricants[serie]) + ", " + str(random.choice(anys[serie])) +"\n"
            file.write(line)    

# Generem les taules en_servei i fora_servei amb les unitats que acabem de crear

causes = [  "Error electromecànica",
            "Fi de vida útil",
            "Cal retornejar les rodes",
            "Fallada bojie de la unitat motriu frontal",
            "Fallada bojie de la unitat motriu posterior",
            "Fallada bojie de la unitat central",
            "Taller de pintura",
            "Taller de xapa",
            "Canvi líquid frens",
            "Restauració",
            "Fallada del sistema de climatització",
            "Fallada de comunicació TIERRA-TREN",
            "Manteniment pantògrafs"
]

en_servei = set()


with open('fora_servei.csv', 'w') as file:
    file.write('id_unitat, motiu, data_baixa\n')
with open('en_servei.csv', 'w') as file:
    file.write('id_unitat, data_ultima_revisio, data_prevista_baixa\n')


for j in series:
    unsorted_serie = unit_series[j]
    out = round(random.uniform(0.1,0.35)*len(unsorted_serie))#Decidim un percentatge de unitats inactives
    
    #En el cas de no tenir moltes unitats actives d'una serie les desactivem totes
    if (len(unsorted_serie)-out) <= defect_min_units_serie:
        out = len(unsorted_serie)
        
        
    for i in range(len(unsorted_serie)):
        if i < out:
            with open('fora_servei.csv', 'a') as file:
                start_date = date(2008, 1, 1).toordinal()
                end_date = date.today().toordinal()
                
                random_date = date.fromordinal(random.randint(start_date, end_date))
                
                line = unsorted_serie[i] + ", " + random.choice(causes) +  ", " + str(random_date) + "\n"
                file.write(line)
                
        else:
            with open('en_servei.csv', 'a') as file:
    
                en_servei.add(unsorted_serie[i])
    
                #Inventem data prevista de baixa
                start_date = date.today().toordinal()
                end_date = date(2030, 12, 31).toordinal()
                ordinal_baixa = random.randint(start_date, end_date)
                random_baixa= date.fromordinal(ordinal_baixa)
    
                #Inventem una data aleatòria de revisió dels últims 2-3 anys
                start_date = date(2019, 1, 1).toordinal()
                end_date = date.today().toordinal()
                ordinal_revisio = random.randint(start_date, end_date)
                random_revisio = date.fromordinal(min(ordinal_baixa, ordinal_revisio)) #Perque no pugui quedar la revisio despres de la baixa
    
                line = unsorted_serie[i] + ", " + str(random_revisio)  + ", " + str(random_baixa) + "\n"
                file.write(line)


#Generem trens aleatòriament fent que sempre siguin de la mateixa sèrie
trens = set()
with open('trens_unitats.csv', 'w') as file:
    file.write('id_tren, id_unitat\n')
    for serie in series:
    
        units = [ x for x in unit_series[serie] if x in en_servei] #Per construir els trens agafem només les unitats que estan en servei
        random.shuffle(units)
        while len(units):
            unit_id = rstr.xeger(r'[0-9]{2}X0[A-Z]')   #Generem un nom aleatori pel tren
            while unit_id in trens:
                unit_id = rstr.xeger(r'[0-9]{2}X0[A-Z]')
            trens.add(unit_id)
            
            for i in range(min(random.randint(1,3), len(units))):  #N'agafem un o dos aleatoriament (si en queda més d'un)
                file.write(unit_id + ", " + units.pop() + "\n")
                    
#Generem el csv de trens
with open('trens.csv','w') as file:
    file.write('id_tren'+'\n')
    for tren in trens:
        file.write(tren+'\n')
        
                
                
                
            
