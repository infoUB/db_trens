import random
import csv
from datetime import date

random.seed(3)

names =['Antonio','David','Francesc','Eric','Carlos','Maria','Anna','Laura','Paula','Sara']
surnames1 =['Martínez','Sánchez','López','García']
surnames2 =['Ferrer','Fuster','Vilà','Serra']
letters=['H','E','Z','A','F','T','B']
profs=['Maquinista','Revisor','Seguretat', 'Manteniment']
profNames = ['Martí','Ricardo','Laia']
base = 39000000
civilCount = 1;
profCount = 1
general=[]
civils=[]
profesionals=[]
taken = set()

dnis = []

for i in range(20):
    newDNI = str(base+random.randint(0,2000000))+letters[random.randint(0,len(letters)-1)]
    while newDNI in taken:
        newDNI = str(base+random.randint(0,2000000))+letters[random.randint(0,len(letters)-1)]
    dnis.append(newDNI)

with open('persones.csv', 'w') as file:
    for i in range(len(dnis)):
        nom= random.choice(names)+' '+random.choice(surnames1)+' '+ random.choice(surnames2)
        if(i<12):
            start_date = date(1940, 1, 1).toordinal()
            end_date = date(2018, 1, 1).toordinal()
        else:
            start_date = date(1956, 1, 1).toordinal()
            end_date = date(2004, 1, 1).toordinal()
            
        dob = date.fromordinal(random.randint(start_date, end_date))

        file.write(dnis[i]+', '+nom+', '+str(dob)+'\n')


with open('clients.csv', 'w') as file:
    file.write("id_clients,\n")
    for i in range(0,11):
        file.write(dnis[i]+',\n')

with open('professionals.csv', 'w') as file:
    file.write("id_clients, tipus\n")
    for i in range(12,20):
        feina = random.choice(profs)
        file.write(dnis[i]+', '+feina+'\n')